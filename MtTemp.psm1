

$script:mOutput = "$PSScriptRoot\multitran.txt"
$script:wClient = New-Object System.Net.WebClient
# Add-Type -AssemblyName "System.Web"

function Get-MyMultitran
{
  param(
    [Parameter(Mandatory)]
    [String] $SourceText,
    [Parameter()]
    [Switch] $rusEng
  )
  try
  {
    if ($rusEng)
    {
      $script:mURL = 'https://www.multitran.com/m.exe?l1=2&l2=1&s='
    }
    else
    {
      $script:mURL = 'https://www.multitran.com/m.exe?l1=1&l2=2&s='
    }

    # $script:SourceEncoded = [System.Web.HttpUtility]::UrlEncode($SourceText)
    $script:SourceEncoded = $SourceText -replace ' ', '+'
    $script:completeURL = $script:mURL + $script:SourceEncoded
    $script:wClient.DownloadFile($completeURL, $mOutput)
    $script:mCounter = 0
    Write-Host -Object "`n$SourceText`n" -ForegroundColor DarkGreen
    Select-String -Path $mOutput -Encoding utf8 -Pattern '<td class="trans".+' |
      ForEach-Object {
        $script:mResult = $PSItem.Matches.Value -replace '<p>', "`n"
        $script:mResult = $script:mResult -replace '<!--.*?-->|<i>.*?</i>;\s<span STYLE="color:rgb\(.*?\)">|<font.*?>.*?</font>|\s<span style="color:gray">\(<i>.*?</i>\)|<.*?>|&nbsp;[^)]+'
        if ($script:mCounter -band 1)
        {
          Write-Host -Object $script:mResult -ForegroundColor DarkBlue
        }
        else
        {
          Write-Host -Object $script:mResult -ForegroundColor Black
        }
        $script:mCounter++
      }
      Write-Host -Object ''
  }
  catch
  {
    Write-Host "`nThe multitran website is temporarily unavailable.`n" -ForegroundColor DarkRed
  }
}

Export-ModuleMember -Function Get-MyMultitran

